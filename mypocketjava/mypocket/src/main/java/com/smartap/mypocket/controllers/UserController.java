package com.smartap.mypocket.controllers;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smartap.mypocket.controllers.util.MappingUtils;
import com.smartap.mypocket.dto.UserDto;
import com.smartap.mypocket.entities.Budget;
import com.smartap.mypocket.entities.User;
import com.smartap.mypocket.service.UserService;

@RestController
@RequestMapping("/mypocket/user")
public class UserController {
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/addUserBudgetSettings", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> addUserBudgetSettings(@RequestBody UserDto userDto) {
		User currentUser = userService.findByUsername(userDto.getUsername());
		currentUser.setSalary(userDto.getSalary());
		currentUser.setSalaryDate(userDto.getSalaryDate());

		long savedAmount = 0;
		if (userDto.getSavedAmount() != null) {
			savedAmount = (long) userDto.getSavedAmount();
		}

		Budget budget = new Budget();
		budget.setDate(LocalDate.now()); // current month
		budget.setAmount(userDto.getSalary() - savedAmount);// amount se schimba pe parcurs cand face cheltuieli in luna
															// resp
		budget.setSavedAmount(savedAmount); // cat salveaza lunar
		budget.setUser(currentUser);

		currentUser.getAmounts().add(budget);
		userService.save(currentUser); // face update

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDto> getUser(@PathVariable(name = "username") String username) {
		User user = userService.findByUsername(username);
		if (user != null) {
			UserDto userDto = MappingUtils.mapUser(user);
			if (userDto != null) {
				return new ResponseEntity<UserDto>(userDto, HttpStatus.OK);
			}
		}
		return new ResponseEntity<UserDto>(HttpStatus.NOT_FOUND);

	}
}
