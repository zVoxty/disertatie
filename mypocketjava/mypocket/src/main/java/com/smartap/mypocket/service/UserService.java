package com.smartap.mypocket.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.smartap.mypocket.entities.User;
import com.smartap.mypocket.generics.GenericServiceImpl;
import com.smartap.mypocket.repository.UserRepository;

@Service
@Transactional
public class UserService extends GenericServiceImpl<User>  implements UserDetailsService{
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	protected UserService(UserRepository userRepository) {
		super(userRepository);
	}
	
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return this.userRepository.findByUsername(username);
	}
	
	public User findByUsername(String username)  {
		return this.userRepository.findByUsername(username);
	}
	
	public void register(User user) {
		String password=user.getPassword();
		user.setPassword(this.passwordEncoder.encode(password));
		userRepository.save(user);
	}
}
