package com.smartap.mypocket.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.smartap.mypocket.entities.Expense;
import com.smartap.mypocket.entities.ExpenseCategory;
import com.smartap.mypocket.entities.ExpenseStatus;
import com.smartap.mypocket.entities.User;
import com.smartap.mypocket.security.auth.jwt.JwtTokenProvider;
import com.smartap.mypocket.service.ExpenseCategoryService;
import com.smartap.mypocket.service.ExpenseService;
import com.smartap.mypocket.service.UserService;

@RestController
@RequestMapping("/mypocket/user/budget")
public class BudgetController {

	@Autowired
	private UserService userService;

	@Autowired
	private ExpenseCategoryService expenseCategoryService;

	@Autowired
	private ExpenseService expenseService;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@RequestMapping(value = "/spentAmount/{categoryId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Long getSpentAmountOnCategory(@PathVariable("categoryId") Long categoryId) {

		ExpenseCategory expenseCategory = expenseCategoryService.findById(categoryId);
		List<Expense> expenses = expenseService.findByCategoryAndStatusAndUser(expenseCategory, ExpenseStatus.PAID,
				getUser());
		Long spentAmount = new Long(0);

		for (Expense expense : expenses) {
			if (expense.getPaymentDate().getMonth().equals(LocalDate.now().getMonth())) {
				spentAmount = spentAmount + expense.getAmount();
			}
		}

		return spentAmount;
	}

	@RequestMapping(value = "/totalSpentAmount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Long getTotalSpentAmount() {
		Long totalSpentAmount = new Long(0);
		List<Expense> expenses = expenseService.findByUser(getUser());
		
		for (Expense expense : expenses) {
			if (expense.getPaymentDate()!=null && expense.getPaymentDate().getMonth().equals(LocalDate.now().getMonth())) {
				totalSpentAmount = totalSpentAmount + expense.getAmount();
			}
		}
		return totalSpentAmount;
	}

	private User getUser() {
		String username = jwtTokenProvider.getUsername();
		return userService.findByUsername(username);

	}

}
