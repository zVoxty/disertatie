package com.smartap.mypocket.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartap.mypocket.entities.ExpenseCategory;
import com.smartap.mypocket.entities.User;
import com.smartap.mypocket.generics.GenericServiceImpl;
import com.smartap.mypocket.repository.ExpenseCategoryRepository;
@Service
@Transactional
public class ExpenseCategoryService extends GenericServiceImpl<ExpenseCategory> {
	@Autowired
	private ExpenseCategoryRepository expenseCategoryRepository;
	
	protected ExpenseCategoryService(ExpenseCategoryRepository expenseCategoryRepository ) {
		super(expenseCategoryRepository);
	}
	
	public ExpenseCategory findByDescription(String description) {
		return expenseCategoryRepository.findByDescription(description);
	}

	public ExpenseCategory findByName(String category) {
		return expenseCategoryRepository.findByName(category);
	}
	
	public List<ExpenseCategory> findByUser(User user){
		return expenseCategoryRepository.findByUser(user);
	}

	public ExpenseCategory findById(Long id) {
		return expenseCategoryRepository.findById(id).get();
	}
}
