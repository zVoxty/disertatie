package com.smartap.mypocket.entities;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
public class ExpensePeriod implements Comparable<ExpensePeriod>{
	@Id
	@GeneratedValue
	private Long id;
	
	private LocalDate date; //selected month cu ziua de 1 anul curent
	
	private boolean payed;
	
	@ManyToMany(mappedBy = "expensePeriods")
	private List<Expense> expenses;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public List<Expense> getExpenses() {
		return expenses;
	}

	public void setExpenses(List<Expense> expenses) {
		this.expenses = expenses;
	}

	public boolean isPayed() {
		return payed;
	}

	public void setPayed(boolean payed) {
		this.payed = payed;
	}

	@Override
	public int compareTo(ExpensePeriod arg0) {
		return this.date.compareTo(arg0.date);
	}

	
	
	
}
