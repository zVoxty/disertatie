package com.smartap.mypocket.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartap.mypocket.entities.Expense;
import com.smartap.mypocket.entities.ExpenseCategory;
import com.smartap.mypocket.entities.ExpenseStatus;
import com.smartap.mypocket.entities.User;
import com.smartap.mypocket.generics.GenericServiceImpl;
import com.smartap.mypocket.repository.ExpenseRepository;

@Service
@Transactional
public class ExpenseService extends GenericServiceImpl<Expense> {
	@Autowired
	private ExpenseRepository expenseRepository;

	protected ExpenseService(ExpenseRepository expenseRepository) {
		super(expenseRepository);
	}

	public List<Expense> findByCategoryAndStatusAndUser(ExpenseCategory category, ExpenseStatus status,User user) {
		return expenseRepository.findByCategoryAndStatusAndUser(category, status,user);
	}

	public Expense findById(Long id) {
	return	expenseRepository.findById(id).get();
	}
	
	public List<Expense> findByStatusAndUser(ExpenseStatus status, User user) {
		return expenseRepository.findByStatusAndUser(status,user);
	}
	
	public List<Expense> findByUser(User user){
		return expenseRepository.findByUser(user);
	}
	public List<Expense> findByCategory(ExpenseCategory category){
		return expenseRepository.findByCategory(category);
	}

}
