package com.smartap.mypocket.dto;

import java.time.LocalDate;

public class UserDto {
	private String username;
	private String email;
	private Long salary; 
	private LocalDate salaryDate;
	private Long amount; 
	private Long savedAmount;
	private Long spentAmount;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getSalary() {
		return salary;
	}
	public void setSalary(Long salary) {
		this.salary = salary;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public Long getSavedAmount() {
		return savedAmount;
	}
	public void setSavedAmount(Long savedAmount) {
		this.savedAmount = savedAmount;
	}
	public LocalDate getSalaryDate() {
		return salaryDate;
	}
	public void setSalaryDate(LocalDate salaryDate) {
		this.salaryDate = salaryDate;
	}
	public Long getSpentAmount() {
		return spentAmount;
	}
	public void setSpentAmount(Long spentAmount) {
		this.spentAmount = spentAmount;
	}
}
