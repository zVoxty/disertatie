package com.smartap.mypocket.entities;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "expenses")
public class Expense {
	@Id
	@GeneratedValue
	private Long id;

	private String title; // crud cu expense

	private Boolean mandatory;

	private Integer amount;
	
	private String description;

	@Enumerated(EnumType.STRING)
	private ExpenseStatus status;
	
	private LocalDate paymentDate;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "expenses_periods", joinColumns = { @JoinColumn(name = "expense_id") }, inverseJoinColumns = {
			@JoinColumn(name = "period_id") })
	private List<ExpensePeriod> expensePeriods; 
	
	@ManyToOne
	@JoinColumn(name = "category_id")
	private ExpenseCategory category;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Expense() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Boolean getMandatory() {
		return mandatory;
	}

	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public ExpenseStatus getStatus() {
		return status;
	}

	public void setStatus(ExpenseStatus status) {
		this.status = status;
	}

	public List<ExpensePeriod> getExpensePeriods() {
		return expensePeriods;
	}

	public void setExpensePeriods(List<ExpensePeriod> expensePeriods) {
		this.expensePeriods = expensePeriods;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ExpenseCategory getCategory() {
		return category;
	}

	public void setCategory(ExpenseCategory category) {
		this.category = category;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
