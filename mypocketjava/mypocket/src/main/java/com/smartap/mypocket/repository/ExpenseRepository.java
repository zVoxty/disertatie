package com.smartap.mypocket.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.smartap.mypocket.entities.Expense;
import com.smartap.mypocket.entities.ExpenseCategory;
import com.smartap.mypocket.entities.ExpenseStatus;
import com.smartap.mypocket.entities.User;
@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Long>{
	public List<Expense> findByCategoryAndStatusAndUser(ExpenseCategory category,ExpenseStatus status, User user);
	public List<Expense> findByStatusAndUser(ExpenseStatus status,User user);
	public List<Expense> findByUser(User user);
	public List<Expense> findByCategory(ExpenseCategory category);

}
