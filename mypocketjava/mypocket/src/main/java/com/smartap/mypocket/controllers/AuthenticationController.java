package com.smartap.mypocket.controllers;

import static org.springframework.http.ResponseEntity.ok;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smartap.mypocket.entities.AuthenticationRequest;
import com.smartap.mypocket.entities.ExpenseCategory;
import com.smartap.mypocket.entities.User;
import com.smartap.mypocket.security.auth.jwt.JwtTokenProvider;
import com.smartap.mypocket.service.ExpenseCategoryService;
import com.smartap.mypocket.service.UserService;


@RestController
@RequestMapping("/mypocket")
public class AuthenticationController {
	@Autowired
	private UserService users;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtTokenProvider jwtTokenProvider;
	
	@Autowired
	private ExpenseCategoryService expenseCategoryService;

	@PostMapping("/auth/register")
	public ResponseEntity<Void> register(@RequestBody User user) {
		ExpenseCategory expenseCategory1 = new ExpenseCategory("Home maintenance", "Short description");
		expenseCategory1.setUser(user);
		ExpenseCategory expenseCategory2 = new ExpenseCategory("Home decoration", "Short description");
		expenseCategory2.setUser(user);
		ExpenseCategory expenseCategory3 = new ExpenseCategory("Health", "Short description");
		expenseCategory3.setUser(user);
		ExpenseCategory expenseCategory4 = new ExpenseCategory("Food", "Short description");
		expenseCategory4.setUser(user);
		ExpenseCategory expenseCategory5 = new ExpenseCategory("Clothes", "Short description");
		expenseCategory5.setUser(user);
		ExpenseCategory expenseCategory6 = new ExpenseCategory("Knowledge", "Short description");
		expenseCategory6.setUser(user);

		expenseCategoryService.save(expenseCategory1);
		expenseCategoryService.save(expenseCategory2);
		expenseCategoryService.save(expenseCategory3);
		expenseCategoryService.save(expenseCategory4);
		expenseCategoryService.save(expenseCategory5);
		expenseCategoryService.save(expenseCategory6);

//		List<ExpenseCategory> categories=new ArrayList<>();
//		categories.addAll(Arrays.asList(expenseCategory1,expenseCategory2,expenseCategory3,expenseCategory4,expenseCategory5,expenseCategory6));
//		
//		user.setCategories(categories);
		
		users.register(user);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@PostMapping("/auth/login")
	public  ResponseEntity<Map<String,String>> login(@RequestBody AuthenticationRequest data) {
		try {
			String username = data.getUsername();
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, data.getPassword()));
			String token = jwtTokenProvider.createToken(username);
			Map<String , String> model = new HashMap<>();
			model.put("username", username);
			model.put("token", token);
			jwtTokenProvider.setAuthToken(token);
			return ok(model);
		} catch (AuthenticationException e) {
			throw new BadCredentialsException("Invalid username/password supplied");
		}
	}
}
