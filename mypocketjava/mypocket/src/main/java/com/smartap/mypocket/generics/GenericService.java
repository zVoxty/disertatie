package com.smartap.mypocket.generics;

import java.util.List;

public interface GenericService<S> {
	public S save(S entity);
	public List<S> findAll();
	void deleteById(Long id);
}
