package com.smartap.mypocket.entities;

public enum ExpenseStatus {
	PAID("Paid"),UNPAID("Unpaid");
	
	private String status;
	ExpenseStatus(String status) {
		this.status=status;
	}
	
	public String getName() {
		return status;
	}
}
