package com.smartap.mypocket.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.smartap.mypocket.entities.ExpenseCategory;
import com.smartap.mypocket.entities.User;

public interface ExpenseCategoryRepository extends JpaRepository<ExpenseCategory, Long>{
	public ExpenseCategory findByDescription(String description);
	public ExpenseCategory findByName(String category);
	public List<ExpenseCategory> findByUser(User user);
}
