package com.smartap.mypocket.generics;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

public class GenericServiceImpl<S> implements GenericService<S> {

	@Autowired
	private JpaRepository<S, Long> jpaRepository;

	protected GenericServiceImpl(JpaRepository<S, Long> jpaRepository) {
		this.jpaRepository = jpaRepository;
	}

	@Override
	public S save(S entity) {
		return jpaRepository.save(entity);
	}

	@Override
	public void deleteById(Long id) {
		jpaRepository.deleteById(id);

	}

	@Override
	public List<S> findAll() {
		return jpaRepository.findAll();
	}
}
