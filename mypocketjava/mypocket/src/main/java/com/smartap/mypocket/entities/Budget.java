package com.smartap.mypocket.entities;

import java.time.LocalDate;
import java.time.Month;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Budget {
	@Id
	@GeneratedValue
	private Long id;
	private Long amount; //suma care ramane din salariu - constraints sa nu fie mai mica decat 0
	private Long savedAmount;
	private Long spentAmount;
	private LocalDate date;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	
	public Budget(){
		this.spentAmount=new Long(0);
		this.savedAmount=new Long(0);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	
	public Long getSavedAmount() {
		return savedAmount;
	}

	public void setSavedAmount(Long savedAmount) {
		this.savedAmount = savedAmount;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getSpentAmount() {
		return spentAmount;
	}

	public void setSpentAmount(Long spentAmount) {
		this.spentAmount = spentAmount;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}
}
