package com.smartap.mypocket.dto;

import java.time.LocalDate;

public class ExpensePeriodDto {
	private Long id;
	private LocalDate date;
	private boolean payed;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public boolean isPayed() {
		return payed;
	}
	public void setPayed(boolean payed) {
		this.payed = payed;
	}
}
