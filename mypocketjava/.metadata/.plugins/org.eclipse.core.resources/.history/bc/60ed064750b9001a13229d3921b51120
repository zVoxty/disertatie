package com.smartap.mypocket.controllers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.smartap.mypocket.controllers.util.MappingUtils;
import com.smartap.mypocket.dto.ExpenseDto;
import com.smartap.mypocket.dto.PaymentDto;
import com.smartap.mypocket.entities.Budget;
import com.smartap.mypocket.entities.Expense;
import com.smartap.mypocket.entities.ExpenseCategory;
import com.smartap.mypocket.entities.ExpensePeriod;
import com.smartap.mypocket.entities.ExpenseStatus;
import com.smartap.mypocket.entities.User;
import com.smartap.mypocket.security.auth.jwt.JwtTokenProvider;
import com.smartap.mypocket.service.ExpenseCategoryService;
import com.smartap.mypocket.service.ExpenseService;
import com.smartap.mypocket.service.UserService;

@RestController
@RequestMapping("/mypocket/expense")
public class ExpenseController {
	@Autowired
	private ExpenseService expenseService;

	@Autowired
	private UserService userService;

	@Autowired
	private ExpenseCategoryService expenseCategoryService;
	
	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> save(@RequestBody Expense expense) {
		User user = userService.findByUsername(expense.getUser().getUsername());
		expense.setUser(user);
		
		ExpenseCategory category = expenseCategoryService.findById(expense.getExpenseCategory().getId());
		expense.setExpenseCategory(category);
		
		List<Expense> expenses = user.getExpenses();
		if (CollectionUtils.isEmpty(expenses)) {
			expenses = new ArrayList<>();
			expenses.add(expense);
			user.setExpenses(expenses);
		} else {
			user.getExpenses().add(expense);
		}

		expenseService.save(expense);
		
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/findAll/{category}/{status}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ExpenseDto> findByCategoryAndStatus(@PathVariable(name = "category") String category,
			@PathVariable(name = "status") String status){
		String username=jwtTokenProvider.getUsername();
		
		ExpenseCategory expenseCategory = expenseCategoryService.findByName(category);
		ExpenseStatus statusEnum = getMappedExpenseStatus(status);

		User user = userService.findByUsername(username);
		List<ExpenseDto> expensesDto = new ArrayList<>();
		List<Expense> expenses=expenseService.findByCategoryAndStatusAndUser(expenseCategory, statusEnum, user);
		
		for (Expense expense :expenses) {
			expensesDto.add(MappingUtils.mapExpense(expense));
		}
		
		return expensesDto;
	}
	
	@RequestMapping(value = "/username", method = RequestMethod.GET)
    @ResponseBody
    public String currentUserName(Authentication authentication) {
        return authentication.getName();
    }

	@RequestMapping(value = "/makePayment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> makePayment(@RequestBody PaymentDto paymentDetails) {
		String username=jwtTokenProvider.getUsername();
		
		User user = userService.findByUsername(username);
		List<Budget> amounts = user.getAmounts();
		Budget lastBudget = amounts.get(amounts.size() - 1);

		lastBudget.setSpentAmount(lastBudget.getAmount() + paymentDetails.getAmount());
		lastBudget.setAmount(lastBudget.getAmount() - paymentDetails.getAmount());
		lastBudget.setDate(LocalDate.now()); 
		
		user.setAmounts(amounts);
		userService.save(user);
		
		Expense currentExpense = expenseService.findById(paymentDetails.getExpenseId());
		currentExpense.setPaymentDate(LocalDate.now());
		currentExpense.setStatus(ExpenseStatus.PAID);
		
		expenseService.save(currentExpense);
		return new ResponseEntity<Void>(HttpStatus.OK);

	}

	@RequestMapping(value = "/delete/{expenseId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> deleteExpense(@PathVariable("expenseId") Long expenseId){	
		String username=jwtTokenProvider.getUsername();
		
		Expense expense = expenseService.findById(expenseId);
		
		User currentUser = userService.findByUsername(username);
		currentUser.getExpenses().remove(expense);
		userService.save(currentUser);
		
		expenseService.deleteById(expenseId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/mandatoryUnpaidExpensesAfterPaymendDate/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ExpenseDto> getMandatoryUnpaidExpensesAfterPaymendDate(@PathVariable("username") String username) {
		List<ExpenseDto> unpaidMandatoryExpensesAfterPaymentDate = new ArrayList<>();
			User user = userService.findByUsername(username);

		List<Expense> expenses = expenseService.findByStatusAndUser(ExpenseStatus.UNPAID, user);
		for (Expense expense : expenses) {
			for (ExpensePeriod period : expense.getExpensePeriods()) {
				if (LocalDate.now().isEqual(period.getDate().minusDays(5))) { 
					unpaidMandatoryExpensesAfterPaymentDate.add(MappingUtils.mapExpense(expense));
				}
			}	
		}
			
		return unpaidMandatoryExpensesAfterPaymentDate;
	}
	
	@RequestMapping(value = "/date/{selectedDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<ExpenseDto> getExpensesByDate(@PathVariable("selectedDate") String date) {
		String username=jwtTokenProvider.getUsername();
		User user = userService.findByUsername(username);

		List<ExpenseDto> expensesByDate=new ArrayList<>();
		List<Expense> expenses = expenseService.findByStatusAndUser(ExpenseStatus.PAID, user);
		
		LocalDate localDate=LocalDate.parse(date);
		for(Expense expense:expenses) {
			if(expense.getPaymentDate().isBefore(localDate) || expense.getPaymentDate().isEqual(localDate)) {
				expensesByDate.add(MappingUtils.mapExpense(expense));
			}
		}

			
		return expensesByDate;
	}

		
	private ExpenseStatus getMappedExpenseStatus(String value) {
		ExpenseStatus status = null;
		switch (value) {
		case "Paid":
			status = ExpenseStatus.PAID;
			break;
		case "Unpaid":
			status = ExpenseStatus.UNPAID;
			break;
		}
		return status;
	}
}
