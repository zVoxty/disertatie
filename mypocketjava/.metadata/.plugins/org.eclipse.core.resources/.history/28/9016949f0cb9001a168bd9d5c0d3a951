package com.smartap.mypocket.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.smartap.mypocket.controllers.util.MappingUtils;
import com.smartap.mypocket.dto.ExpenseCategoryDto;
import com.smartap.mypocket.entities.ExpenseCategory;
import com.smartap.mypocket.entities.User;
import com.smartap.mypocket.security.auth.jwt.JwtTokenProvider;
import com.smartap.mypocket.service.ExpenseCategoryService;
import com.smartap.mypocket.service.UserService;

@RestController
@RequestMapping("/mypocket/expense/categories")
public class ExpenseCategoryController {
	@Autowired
	private ExpenseCategoryService expenseCategoryService;
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ExpenseCategoryDto> getExpenseCategories() {
		String username = jwtTokenProvider.getUsername();
		User user = userService.findByUsername(username);
		
		ArrayList<ExpenseCategoryDto> expenseCategories = new ArrayList<>();

		for (ExpenseCategory expenseCategory : expenseCategoryService.findByUser(user)) {
			expenseCategories.add(MappingUtils.mapExpenseCategory(expenseCategory));
		}

		return expenseCategories;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HttpStatus addExpenseCategory(@RequestBody ExpenseCategoryDto expenseCategoryDto) {
		ExpenseCategory category = new ExpenseCategory();
		category.setName(expenseCategoryDto.getName());
		category.setDescription(expenseCategoryDto.getDescription());
		category.setName(expenseCategoryDto.getName());

		expenseCategoryService.save(category);

		return HttpStatus.CREATED;
	}

	@RequestMapping(value = "/delete/{categoryName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HttpStatus deleteExpenseCategory(@PathVariable("categoryName") String name) {
		String username = jwtTokenProvider.getUsername();
		User user = userService.findByUsername(username);

		ExpenseCategory category = expenseCategoryService.findByName(name);
		expenseCategoryService.deleteById(category.getId());
		return HttpStatus.OK;
	}

}
