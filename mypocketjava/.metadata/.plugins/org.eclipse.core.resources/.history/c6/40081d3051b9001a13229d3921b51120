package com.smartap.mypocket.controllers.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.smartap.mypocket.dto.ExpenseCategoryDto;
import com.smartap.mypocket.dto.ExpenseDto;
import com.smartap.mypocket.dto.ExpensePeriodDto;
import com.smartap.mypocket.dto.UserDto;
import com.smartap.mypocket.entities.Budget;
import com.smartap.mypocket.entities.Expense;
import com.smartap.mypocket.entities.ExpenseCategory;
import com.smartap.mypocket.entities.ExpensePeriod;
import com.smartap.mypocket.entities.User;

public class MappingUtils {
	public static String authToken;
	
	public static UserDto mapUser(User user) {
		UserDto userDto = new UserDto();
		userDto.setUsername(user.getUsername());
		userDto.setEmail(user.getEmail());
		if (user.getSalary() != null) {
			userDto.setSalary(user.getSalary());
			userDto.setSalaryDate(user.getSalaryDate());

			List<Budget> amounts = user.getAmounts();
			
			if (!CollectionUtils.isEmpty(amounts)) {
				Budget lastBudget = amounts.get(amounts.size() - 1);
				userDto.setAmount(lastBudget.getAmount()); // cat mai e din salariu
				userDto.setSavedAmount(lastBudget.getSavedAmount());
				userDto.setSpentAmount(lastBudget.getSpentAmount());// cat a platit pana in ziua respectiva
			}
		}
		
		return userDto;
	}

	public static ExpenseCategoryDto mapExpenseCategory(ExpenseCategory expenseCategory) {
		ExpenseCategoryDto expenseCategoryDto = new ExpenseCategoryDto();
		expenseCategoryDto.setId(expenseCategory.getId());
		expenseCategoryDto.setName(expenseCategory.getName());
		expenseCategoryDto.setDescription(expenseCategory.getDescription());
		return expenseCategoryDto;
	}

	public static ExpenseDto mapExpense(Expense expense) {
		ExpenseDto expenseDto = new ExpenseDto();
		expenseDto.setId(expense.getId());
		expenseDto.setTitle(expense.getTitle());
		expenseDto.setAmount(expense.getAmount());
		expenseDto.setMandatory(expense.getMandatory());
		expenseDto.setUsername(expense.getUser().getUsername());
		expenseDto.setExpenseCategory(mapExpenseCategory(expense.getCategory())); //verificat daca merge
		expenseDto.setStatus(expense.getStatus().getName());
		expenseDto.setDescription(expense.getDescription());
		expenseDto.setPaymentDate(expense.getPaymentDate());
		List<ExpensePeriodDto> expensePeriods = new ArrayList<>();
		
		for (ExpensePeriod expensePeriod : expense.getExpensePeriods()) {
			ExpensePeriodDto expensePeriodDto = new ExpensePeriodDto();
			expensePeriodDto.setId(expensePeriod.getId());
			expensePeriodDto.setDate(expensePeriod.getDate());
			expensePeriods.add(expensePeriodDto);
		}
		
		expenseDto.setExpensePeriods(expensePeriods);
		return expenseDto;
	}

	public static String getAuthToken() {
		return authToken;
	}

	public static void setAuthToken(String authToken) {
		MappingUtils.authToken = authToken;
	}
}
