import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SideMenuPage } from './components/side-menu/side-menu.page';
import { SideMenuPageModule } from './components/side-menu/side-menu.module';
import { WelcomeSettingsPage } from './components/welcome-settings/welcome-settings.page';
import { WelcomeSettingsPageModule } from './components/welcome-settings/welcome-settings.module';
import { UserBudgetModalPage } from './components/modals/user-budget-modal/user-budget-modal.page';
import { UserBudgetModalPageModule } from './components/modals/user-budget-modal/user-budget-modal.module';
import { DataTransferService } from './services/data-transfer.service';
import { HistoryPageModule } from './components/history/history.module';
import { HistoryPage } from './components/history/history.page';


@NgModule({
  declarations: [AppComponent, UserBudgetModalPage],
  entryComponents: [SideMenuPage, WelcomeSettingsPage],
  imports: [BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    FormsModule,
    SideMenuPageModule,
    WelcomeSettingsPageModule,
    UserBudgetModalPageModule],

  providers: [
    StatusBar,
    SplashScreen,
    DataTransferService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
