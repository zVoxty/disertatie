import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./components/authentication/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./components/authentication/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'popover',
    loadChildren: () => import('./components/modals/popover/popover.module').then( m => m.PopoverPageModule)
  },
  {
    path: 'expense-management',
    loadChildren: () => import('./components/expense-management/expense-management.module').then( m => m.ExpenseManagementPageModule)
  },
  {
    path: 'expense-statuses-card/:name',
    loadChildren: () => import('./components/expense-statuses-card/expense-statuses-card.module').then( m => m.ExpenseStatusesCardPageModule)
  },
  {
    path: 'expense-statuses-card',
    loadChildren: () => import('./components/expense-statuses-card/expense-statuses-card.module').then( m => m.ExpenseStatusesCardPageModule)
  },
  {
    path: 'side-menu',
    loadChildren: () => import('./components/side-menu/side-menu.module').then( m => m.SideMenuPageModule)
  },
  {
    path: 'welcome-settings',
    loadChildren: () => import('./components/welcome-settings/welcome-settings.module').then( m => m.WelcomeSettingsPageModule)
  },
  {
    path: 'user-budget-modal',
    loadChildren: () => import('./components/modals/user-budget-modal/user-budget-modal.module').then( m => m.UserBudgetModalPageModule)
  },
  {
    path: 'date-picker',
    loadChildren: () => import('./components/date-picker/date-picker.module').then( m => m.DatePickerPageModule)
  },
  {
    path: 'add-expense',
    loadChildren: () => import('./components/add-expense/add-expense.module').then( m => m.AddExpensePageModule)
  },
  {
    path: 'expenses-list',
    loadChildren: () => import('./components/expenses-list/expenses-list.module').then( m => m.ExpensesListPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./components/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'mandatory-expenses',
    loadChildren: () => import('./components/mandatory-expenses/mandatory-expenses.module').then( m => m.MandatoryExpensesPageModule)
  },
  {
    path: 'expense-details',
    loadChildren: () => import('./components/expense-details/expense-details.module').then( m => m.ExpenseDetailsPageModule)
  },
  {
    path: 'history',
    loadChildren: () => import('./components/history/history.module').then( m => m.HistoryPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
