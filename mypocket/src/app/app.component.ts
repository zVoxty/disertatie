import { Component, OnInit } from '@angular/core';
import { SideMenuPage } from './components/side-menu/side-menu.page';
import { UserService } from './services/user.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  rootPage: any = SideMenuPage;

  ngOnInit() {
    this.initialize();
  }

  constructor(private userService: UserService, private alertCtrl: AlertController) {
  }

  async initialize() {
    //se adauga salariul automat pe data de 1 a fiecarei luni
    try {
      let user = await this.userService.getUser();
      if (user != null) {
        if (user.salary != null) {
          this.userService.addSalaryToUser().subscribe(response => {
            this.showSalaryPaymentAlert(); //aici adaug  cu eroare etc ca in Angualar
          })
        }
      }
    } catch (error) {
      console.log("App component failed");
    }
  }

  isSalaryDate(salaryDate: Date): any {
    let currentDate = new Date();
    return salaryDate.getDay == currentDate.getDay;
  }

  async showSalaryPaymentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Salary payment',
      message: 'Your salary was added to your budget!',
      buttons: [
        {
          text: 'Cancel'
        }
        , {
          text: 'Details',
          handler: () => {
            console.log('Showing details about that expenses');
          }
        }
      ]
    });
    await alert.present();
  }

}