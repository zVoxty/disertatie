import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExpenseStatusesCardPage } from './expense-statuses-card.page';

const routes: Routes = [
  {
    path: '',
    component: ExpenseStatusesCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpenseStatusesCardPageRoutingModule {}
