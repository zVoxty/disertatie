import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, NavParams, AlertController, ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { UserBudgetModalPage } from '../modals/user-budget-modal/user-budget-modal.page';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { ExpensesService } from 'src/app/services/expenses.service';
import { DataTransferService } from 'src/app/services/data-transfer.service';
import { Expense } from 'src/app/models/expense';
import { ExpenseCategoryService } from 'src/app/services/expense-category.service';
import { SideMenuPage } from '../side-menu/side-menu.page';

@Component({
  selector: 'app-expense-statuses-card',
  templateUrl: './expense-statuses-card.page.html',
  styleUrls: ['./expense-statuses-card.page.scss'],
})
export class ExpenseStatusesCardPage implements OnInit {
  expenseCategoryName: string;
  expenseCategoryDescription: string;
  expenseCategoryId: number;
  month: string;
  spentAmountPerCategory: number;
 

  constructor(private route: ActivatedRoute, private router: Router, private modalController: ModalController,
    private userService: UserService, private expenseService: ExpensesService, private dataTransferService: DataTransferService,
    private alertController: AlertController, private expenseCategoryService: ExpenseCategoryService,private sideMenu:SideMenuPage) {
     
    
  }
  ngOnInit() {
    this.route.queryParams
    .subscribe(params => {
      this.expenseCategoryName = params.expenseCategoryName;
      this.expenseCategoryDescription=params.expenseCategoryDescription;
      this.expenseCategoryId=params.expenseCategoryId;

      this.spentAmountPerCategory=params.spentAmountPerCategory;
    });
    let currentDate = new Date();
    this.month = currentDate.toLocaleString('en-EN', { month: 'long' })
  }

  showExpenses(selectedExpenseStatus: string) {
    this.router.navigate(['/expenses-list'],
      {
        queryParams: {
          expensesCategoryName: this.expenseCategoryName,
          expensesCategoryDescription:this.expenseCategoryDescription,
          expensesCategoryId: this.expenseCategoryId,
          expensesStatus: selectedExpenseStatus
        }
      });

  } 

  async showCurrentUserBudgetModal() {
    const modal = await this.modalController.create({
      component: UserBudgetModalPage
    });
    return await modal.present();
  }

  openExpenseManagementPage() {
    this.router.navigate(['/expense-management'],
      { queryParams: 
        { expensesCategoryName: this.expenseCategoryName, 
          expensesCategoryDescription: this.expenseCategoryDescription,
          expensesCategoryId: this.expenseCategoryId,
          expensesStatus: 'Unpaid'
        } });
  }

  getSpentAmountOnCategory() {
    this.expenseService.getSpentAmountOnCategory(this.expenseCategoryId).subscribe(spentAmount => {
      this.spentAmountPerCategory = spentAmount;
    })
  }

  getSpentCategoryAmount() {
    return this.spentAmountPerCategory ? this.spentAmountPerCategory : 0;
  }

  async removeCategory() {
    const alert = await this.alertController.create({
      header: 'Remove category',
      message: 'Do you want to remove this category?',
      buttons: [
        {
          text: 'No',
          role: 'no',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Yes',
          handler:() => {
            this.expenseCategoryService.deleteExpenseCategory(this.expenseCategoryId).subscribe(response=>{
              if(response==='OK'){
                this.sideMenu.ngOnInit();
                this.router.navigate(['/home']);
              }
            })
          }
        }
      ]
    });
    await alert.present();
  }

  showHistory(){
    this.router.navigate(['/history']);
  }
}
