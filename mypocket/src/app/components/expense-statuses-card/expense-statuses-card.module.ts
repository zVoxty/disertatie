import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExpenseStatusesCardPageRoutingModule } from './expense-statuses-card-routing.module';

import { ExpenseStatusesCardPage } from './expense-statuses-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExpenseStatusesCardPageRoutingModule
  ],
  declarations: [ExpenseStatusesCardPage],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ExpenseStatusesCardPageModule {}
