import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DataTransferService } from 'src/app/services/data-transfer.service';
import { AlertController } from '@ionic/angular';
import { ExpensesService } from 'src/app/services/expenses.service';
import { Router } from '@angular/router';
import { Expense } from 'src/app/models/expense';
import { UserService } from 'src/app/services/user.service';
import { ExpenseCategoryService } from 'src/app/services/expense-category.service';
import { ExpenseCategory } from 'src/app/models/expensCategory';
import { BudgetService } from 'src/app/services/budget.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, AfterViewInit {
  expenseCategories: ExpenseCategory[];
  totalSpentAmount:number;

  constructor(private alertCtrl: AlertController,
    private expenseService: ExpensesService,
    private dataTransferService: DataTransferService,
    private router: Router,
    private userService: UserService,
    private expenseCategoryService: ExpenseCategoryService,
    private budgetService: BudgetService) {
    this.getMandatoryUnpaidExpensesAfterPaymendDate();
  }

  async ngAfterViewInit() {
    await this.userService.getUser();
    this.budgetService.getTotalSpentAmount().subscribe(totalSpentAmount=>{              
      if(totalSpentAmount!=null){
        this.dataTransferService.totalSpentAmount=Number(totalSpentAmount.toString());
      }
    })
    this.getExpenseCategories();
  }

  async ngOnInit() {
   
  }

  async showMandatoryUnpaidExpensesAfterPaymendDate() {
    const alert = await this.alertCtrl.create({
      header: 'There are unpaid mandatory expenses!',
      message: 'Five days until you have to make the payment',
      buttons: [
        {
          text: 'Cancel'
        }
        , {
          text: 'Details',
          handler: () => {
            this.router.navigate(['/mandatory-expenses']);
          }
        }
      ]
    });
    await alert.present();
  }

  getMandatoryUnpaidExpensesAfterPaymendDate() {
      this.expenseService.getMandatoryUnpaidExpensesAfterPaymendDate(this.dataTransferService.currUser.username).subscribe(expenses => {
        if(expenses!=null && expenses.length!=0){
        this.dataTransferService.expenses=expenses;
        this.showMandatoryUnpaidExpensesAfterPaymendDate();       
        }
      })
  }

  getUsername() {
    return this.dataTransferService.currUser.username;
  }

  getEmail(){
    return this.dataTransferService.currUser.email;
  }
  getSalaryDate(){
    return this.dataTransferService.currUser.salaryDate;
  }

  getUser() {
    this.dataTransferService.currUser;
  }

  getExpenseCategories() {
    this.expenseCategoryService.getExpenseCategories()
    .subscribe(expenseCategories => this.expenseCategories = expenseCategories);
  }

  userSalary() {
    return this.dataTransferService.currUser.salary;
  }

  userLeftAmount() {
    return this.dataTransferService.currUser.amount;
  }

  userSavedAmount() {
    return this.dataTransferService.currUser.savedAmount;
  }

  userSpentAmount() {
    return this.dataTransferService.totalSpentAmount;
  }
  
}
