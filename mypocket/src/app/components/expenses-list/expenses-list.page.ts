import { Component, OnInit } from '@angular/core';
import { Expense } from 'src/app/models/expense';
import { ActivatedRoute, Router } from '@angular/router';
import { ExpensesService } from 'src/app/services/expenses.service';
import { DataTransferService } from 'src/app/services/data-transfer.service';
import { AlertController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-expenses-list',
  templateUrl: './expenses-list.page.html',
  styleUrls: ['./expenses-list.page.scss'],
})
export class ExpensesListPage implements OnInit {
  expenses = new Array<Expense>();
  selectedExpensesCategoryName;
  selectedExpensesCategoryDescription;
  selectedExpensesCategoryId;
  selectedExpensesStatus;
  isShowDates;
  selectedExpense;
  isDisabled;

  constructor(private route: ActivatedRoute,
    private expensesService: ExpensesService,
    private dataTransferService: DataTransferService,
    private router: Router,
    private alertCtrl: AlertController,
    private userService: UserService) {

  }

  ngOnInit() {
    this.getExpenses();
  }

  deleteExpense(expenseId: number) {
    console.log("expenseId: " + expenseId)

    this.expensesService.deleteExpense(expenseId).subscribe(response => {
      console.log("Expense deleted, with id: " + expenseId)

      this.expensesService.getExpensesByCategoryAndStatus(this.selectedExpensesCategoryName, this.selectedExpensesStatus)
        .subscribe(response => {
          this.expenses = response;
        })
    });

  }

  getExpenses() {
    this.route.queryParams.subscribe(params => {
      this.selectedExpensesCategoryName = params.expensesCategoryName;
      this.selectedExpensesCategoryDescription = params.expensesCategoryDescription;
      this.selectedExpensesCategoryId = params.expensesCategoryId;
      this.selectedExpensesStatus = params.expensesStatus;
      this.expensesService.getExpensesByCategoryAndStatus(this.selectedExpensesCategoryName,
        this.selectedExpensesStatus)
        .subscribe(response => {
          this.expenses = response;
        });

    });
  }

  async makePayment(expense: Expense) {
    const currentDate = new Date();
    debugger
    for (const period of expense.expensePeriods) {
      let date = new Date(period.date)
      if (!period.payed && date.getMonth() != currentDate.getMonth()) {
        this.showCanOnlyPayOnScheduledDateAlert();
        break;
      } else if (!period.payed && date.getMonth() === currentDate.getMonth()) {

        if (this.isExpenseAmountLessOrEqualThanCurrentLeftAmount(expense.amount)) {
          this.selectedExpense = expense;
          this.expensesService.makePayment(expense.id, expense.amount).subscribe(response => {
            this.alertCtrl.create({
              header: 'Payment',
              message: 'Payment successfully made!',
              buttons: [{
                text: 'OK',
                handler: () => {
                  this.ngOnInit();
                }
              }]
            }).then(alert => {
              alert.present()
            });
          })

        } else {

          this.alertCtrl.create({
            header: 'Payment',
            message: 'Your current amount is not sufficient for this payment.',
            buttons: [{
              text: 'Cancel',
              handler: () => {
              }
            },
            {
              text: 'Pay from saved amount',
              handler: () => {
                this.alertCtrl.create({
                  header: 'Payment',
                  message: 'Your saved amount is not sufficient for this payment. Wait until next salary payment.',
                  buttons: [{
                    text: 'OK',
                    handler: () => {
                      this.isDisabled = true;
                    }
                  }]
                }).then(alert => {
                  alert.present()
                });
              }

            }]
          }).then(alert => {
            alert.present()
          });
        }
        break;
      }
    }

  }


  private isExpenseAmountLessOrEqualThanCurrentLeftAmount(expenseAmount) {
    return expenseAmount <= this.dataTransferService.currUser.amount;
  }

  editExpense(expense: Expense) {
    //set selectedDates pt datePicker
    let selectedDates = new Array<Date>();
    expense.expensePeriods.forEach(expensePeriod => {
      selectedDates.push(new Date(expensePeriod.date));
    })

    this.dataTransferService.selectedDates = selectedDates.slice();
    this.dataTransferService.contextExpense = expense;
    this.router.navigate(['/expense-management'],
      {
        queryParams: {
          refresh: Math.floor(Math.random() * Math.floor(100000)), 
          expensesCategoryName: this.selectedExpensesCategoryName,
          expensesStatus: this.selectedExpensesStatus,
          expensesCategoryId: this.selectedExpensesCategoryId,
          editMode: true
        }
      });
  }

  addExpense() {
    this.dataTransferService.contextExpense = new Expense();
    this.router.navigate(['/expense-management'],
      {
        queryParams: {
          refresh: Math.floor(Math.random() * Math.floor(100000)), 
          expensesCategoryName: this.selectedExpensesCategoryName,
          expensesCategoryDescription: this.selectedExpensesCategoryDescription,
          expensesCategoryId: this.selectedExpensesCategoryId,
          expensesStatus: 'Unpaid'
        }
      });
  }

  showDates(expense) {
    if (this.selectedExpense == null) {
      this.selectedExpense = expense;
    } else {
      this.selectedExpense = null;
    }
  }

  private showCanOnlyPayOnScheduledDateAlert() {
    this.alertCtrl.create({
      header: 'Payment not possible',
      message: 'You can only pay on the scheduled day of the expense!',
      buttons: [{
        text: 'OK',
        handler: () => {
        }
      }]

    }).then(alert => {
      alert.present()
    });
  }

}
