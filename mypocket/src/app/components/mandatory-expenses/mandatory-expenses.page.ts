import { Component, OnInit } from '@angular/core';
import { Expense } from '../../models/expense';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTransferService } from 'src/app/services/data-transfer.service';

@Component({
  selector: 'app-mandatory-expenses',
  templateUrl: './mandatory-expenses.page.html',
  styleUrls: ['./mandatory-expenses.page.scss'],
})
export class MandatoryExpensesPage implements OnInit {
  mandatoryUnpaidExpenses: Array<Expense>;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private dataTransferService: DataTransferService) {

  }

  ngOnInit() {
      this.mandatoryUnpaidExpenses = this.dataTransferService.expenses;
  }

  onHold(mandatoryUnpaidExpense: Expense) {
    this.dataTransferService.selectedExpense=mandatoryUnpaidExpense;
    this.router.navigate(['/expense-details']);
  }

}
