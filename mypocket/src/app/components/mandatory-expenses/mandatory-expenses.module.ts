import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MandatoryExpensesPageRoutingModule } from './mandatory-expenses-routing.module';

import { MandatoryExpensesPage } from './mandatory-expenses.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MandatoryExpensesPageRoutingModule
  ],
  declarations: [MandatoryExpensesPage]
})
export class MandatoryExpensesPageModule {}
