import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MandatoryExpensesPage } from './mandatory-expenses.page';

const routes: Routes = [
  {
    path: '',
    component: MandatoryExpensesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MandatoryExpensesPageRoutingModule {}
