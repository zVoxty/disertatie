import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Expense } from 'src/app/models/expense';
import { DataTransferService } from 'src/app/services/data-transfer.service';
import { ExpensesService } from 'src/app/services/expenses.service';
import { AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { debug } from 'util';
import { ExpensePeriod } from 'src/app/models/expensePeriod';

@Component({
  selector: 'app-add-expense',
  templateUrl: './add-expense.page.html',
  styleUrls: ['./add-expense.page.scss'],
})
export class AddExpensePage implements OnInit, OnChanges {
  expense: Expense;
  @Input() selectedExpensesCategoryName;
  @Input() selectedExpensesCategoryDescription;
  @Input() selectedExpensesCategoryId;

  @Input() selectedExpensesStatus;
  @Input() editMode = false;
  @Input() refresh;

  private expensePeriods: Array<ExpensePeriod>;

  constructor(private dataTransferService: DataTransferService,
    private expensesService: ExpensesService,
    private alertCtrl: AlertController,
    private router: Router,
    private userService: UserService) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.dataTransferService.contextExpense) {
      this.expense = new Expense();
    } else {
      this.expense = this.dataTransferService.contextExpense;     
    }
    
    if (!this.expense.id) {
      this.expensePeriods = new Array();
      this.dataTransferService.selectedDates = [];  
    }
  }

  ngOnInit() {
    if (this.dataTransferService.contextExpense) {
      this.expense = this.dataTransferService.contextExpense;
    }
    else {
      this.expense = new Expense();
      this.expensePeriods = new Array();
      this.dataTransferService.selectedDates = [];      
    }
  }

  saveExpense(expense: Expense) {
    this.expense = expense;
    
    // reset expense periods because if expense is sent again it will send more expense periods
    this.expensePeriods = [];

    if (this.dataTransferService.selectedDates.length === 0) {
      return;
    }
    
    this.dataTransferService.selectedDates.forEach(selectedDate => {
      this.expensePeriods.push(new ExpensePeriod(selectedDate));
    });

    if (!this.editMode) {
      this.expense.category.name = this.selectedExpensesCategoryName;
      this.expense.category.description = this.selectedExpensesCategoryDescription;
      this.expense.category.id = this.selectedExpensesCategoryId;
      this.expense.status = this.selectedExpensesStatus;
    }

    this.expense.expensePeriods = this.expensePeriods;

    this.save(this.expense);
  }

  async save(expense: Expense) {
    try {
      let user = await this.userService.getUser();

      expense.status = this.expensesService.getStatusValueForServer(expense.status);
      expense.user = user;

      this.expensesService.save(expense).subscribe(response => {
        if (this.editMode) {
          this.showEditedExpenseAlert();
        } else {
          this.showAddedExpenseAlert();
        }
      })
    } catch (error) {
      console.log("add expense failed")
    }
  }

  async showAddedExpenseAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Added expense',
      message: 'Your expense was successfully added!',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.ngOnInit();
          }
        }
      ]
    });
    await alert.present();
  }
  async showEditedExpenseAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Expense edited',
      message: 'Your expense was successfully edited!',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.getExpenses();
          }
        }
      ]
    });
    await alert.present();
  }

  private getExpenses(){
    this.router.navigate(['/expenses-list'],
    { queryParams: {           
      expensesCategoryName: this.selectedExpensesCategoryName,
      expensesCategoryDescription:this.selectedExpensesCategoryDescription,
      expensesCategoryId:this.selectedExpensesCategoryId,
      expensesStatus: this.selectedExpensesStatus 
    } });
   }


}
