import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/user';
import { ModalController } from '@ionic/angular';
import { DataTransferService } from 'src/app/services/data-transfer.service';


@Component({
  selector: 'app-user-budget-modal',
  templateUrl: './user-budget-modal.page.html',
  styleUrls: ['./user-budget-modal.page.scss'],
})
export class UserBudgetModalPage implements OnInit {
  constructor(private dataTransferService: DataTransferService, private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

  getUsername() {
    return this.dataTransferService.currUser.username;
  }

  userSalary() {
    return this.dataTransferService.currUser.salary;
  }

  userLeftAmount() {
    return this.dataTransferService.currUser.amount;
  }

  userSavedAmount() {
    return this.dataTransferService.currUser.savedAmount;
  }

  userSpentAmount() {
    return this.dataTransferService.currUser.totalSpentAmount ? this.dataTransferService.currUser.totalSpentAmount : 0;
  }
}
