import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserBudgetModalPageRoutingModule } from './user-budget-modal-routing.module';

import { UserBudgetModalPage } from './user-budget-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserBudgetModalPageRoutingModule
  ],
  declarations: []
})
export class UserBudgetModalPageModule {}
