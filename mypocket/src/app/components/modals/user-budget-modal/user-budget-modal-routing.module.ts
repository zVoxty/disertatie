import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserBudgetModalPage } from './user-budget-modal.page';

const routes: Routes = [
  {
    path: '',
    component: UserBudgetModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserBudgetModalPageRoutingModule {}
