import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {

  constructor(public modalCtrl: ModalController , private router: Router) { }
  ngOnInit() {
  }
  
  dismissPopover() {
    this.modalCtrl.dismiss();
  }
  logout() {
    this.modalCtrl.dismiss();
    this.router.navigateByUrl('');
  }




}
