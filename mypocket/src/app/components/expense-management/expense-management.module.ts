import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ExpenseManagementPageRoutingModule } from './expense-management-routing.module';
import { ExpenseManagementPage } from './expense-management.page';
import { DatePickerModule } from 'ionic4-date-picker';
import { DatePickerPage } from '../date-picker/date-picker.page';
import { AddExpensePageModule } from '../add-expense/add-expense.module';
import { AddExpensePage } from '../add-expense/add-expense.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExpenseManagementPageRoutingModule,
    AddExpensePageModule,
    DatePickerModule
  ],
  declarations: [ExpenseManagementPage,DatePickerPage],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class ExpenseManagementPageModule {}
