import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, NavParams, ModalController, AlertController } from '@ionic/angular';
import { Expense } from '../../models/expense';
import { Storage } from '@ionic/storage';
import { ExpensesService } from '../../services/expenses.service';
import { AddExpensePage } from '../add-expense/add-expense.page';


@Component({
  selector: 'app-expense-management',
  templateUrl: './expense-management.page.html',
  styleUrls: ['./expense-management.page.scss'],
})
export class ExpenseManagementPage implements OnInit {
  @ViewChild(AddExpensePage,{static: false}) addExpensePage: AddExpensePage;
  selectedExpensesCategoryName = "";
  selectedExpensesCategoryDescription = "";
  selectedExpensesCategoryId = "";
  selectedExpensesStatus = "";
  addExpense: boolean = false;
  editMode:boolean;
  refresh: any;

  constructor( private route: ActivatedRoute, private storage: Storage,
    private expenseService: ExpensesService,private alertCtrl:AlertController,private router:Router) {
    this.getUrlParmeters();
  }

  ngOnInit() {
  }

  getUrlParmeters() {
    this.route.queryParams
      .subscribe(params => {
        this.refresh = params.refresh;
        this.selectedExpensesStatus = params.expensesStatus;
        this.selectedExpensesCategoryName = params.expensesCategoryName;
        this.selectedExpensesCategoryDescription=params.expensesCategoryDescription;
        this.selectedExpensesCategoryId=params.expensesCategoryId;
        this.editMode=params.editMode;
      });
  }

  blurElements() {
    this.addExpense = true;
  }

 getExpenses(){
  this.router.navigate(['/expenses-list'],
  { queryParams: {           
    expensesCategoryName: this.selectedExpensesCategoryName,
    expensesCategoryDescription:this.selectedExpensesCategoryDescription, 
    expensesCategoryId: this.selectedExpensesCategoryId,
    expensesStatus: this.selectedExpensesStatus 
  } });
 }
}
