import { Component, OnInit } from '@angular/core';
import { Expense } from 'src/app/models/expense';
import { ActivatedRoute } from '@angular/router';
import { DataTransferService } from 'src/app/services/data-transfer.service';

@Component({
  selector: 'app-expense-details',
  templateUrl: './expense-details.page.html',
  styleUrls: ['./expense-details.page.scss'],
})
export class ExpenseDetailsPage implements OnInit {

 expense:Expense;

  constructor(private dataTransferService:DataTransferService,private route:ActivatedRoute) {   }

  ngOnInit() {
    this.expense=this.dataTransferService.expense;
  }

}
