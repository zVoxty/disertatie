import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user';
import { AlertController, ToastController } from '@ionic/angular';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  private user: User;
  submitted = false;
 

  constructor(private auth: AuthService, private router: Router, private alertCtrl: AlertController,
    private toastController: ToastController) {
    this.user = new User();
  }

  ngOnInit() {
  }

  async register() {
    this.submitted = true;
    if (!this.user.username || !this.user.password || !this.user.email) {
      const toast = await this.toastController.create({
        color: "warning",
        message: "Please provide all register information.",
        duration: 2000
      });
      toast.present();
      return;
    }

    this.auth.register(this.user).subscribe(
      data => this.onSuccess(data),
      error => this.handleError(error)
    )
  }

  private async onSuccess(data: any) {
    const alert = await this.alertCtrl.create({
      header: 'Successfully registered!',
      buttons: [{
        text: 'OK',
        handler: () => {
          this.onComplete();
        }
      }]
    });
    await alert.present();
  }

  private async handleError(data: any) {
    const alert = await this.alertCtrl.create({
      header: 'Registration Failed',
      buttons: ['OK']
    });
    await alert.present();
  }

  private onComplete() {
    this.router.navigateByUrl('')
  }

  



}
