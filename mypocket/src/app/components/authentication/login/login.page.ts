import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { AlertController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { User } from '../../../models/user';
import { UserService } from 'src/app/services/user.service';
import { DataTransferService } from 'src/app/services/data-transfer.service';
import { BudgetService } from 'src/app/services/budget.service';
import { SideMenuPage } from '../../side-menu/side-menu.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  private user: User;

  constructor(private auth: AuthService, private router: Router, private alertCtrl: AlertController,
    private userService: UserService, private toastController: ToastController,
    private dataTransferService: DataTransferService,private budgetService:BudgetService, private sideMenu:SideMenuPage) {
    this.user = new User();
  }

  ngOnInit() { }

 async login() {
    // validate inputs
    if (!this.user.username || !this.user.password) {
      const toast = await this.toastController.create({
        color: "warning",
        message: "Please provide all login information.",
        duration: 2000
      });
      toast.present();
      return;
    }

    this.auth.login(this.user.username, this.user.password).subscribe( async res => {
      
       if (res) {
          let user =await this.userService.getUser();
          if (user.salary == null) {
            this.router.navigateByUrl('welcome-settings')
          } else {
            this.budgetService.getTotalSpentAmount().subscribe(totalSpentAmount=>{              
              if(totalSpentAmount!=null){
                this.dataTransferService.totalSpentAmount=Number(totalSpentAmount.toString());
              }
            })
            this.sideMenu.ngOnInit();
            this.router.navigateByUrl('home')
          }
        
      } else {
        const alert = await this.alertCtrl.create({
          header: 'Login Failed',
          message: 'Wrong credentials.',
          buttons: ['OK']
        });
         alert.present();
      }
    });
  }

}
