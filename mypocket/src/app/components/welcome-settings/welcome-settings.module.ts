import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WelcomeSettingsPageRoutingModule } from './welcome-settings-routing.module';

import { WelcomeSettingsPage } from './welcome-settings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WelcomeSettingsPageRoutingModule
  ],
  declarations: [WelcomeSettingsPage]
})
export class WelcomeSettingsPageModule {}
