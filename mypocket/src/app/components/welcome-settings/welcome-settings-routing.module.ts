import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeSettingsPage } from './welcome-settings.page';

const routes: Routes = [
  {
    path: '',
    component: WelcomeSettingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WelcomeSettingsPageRoutingModule {}
