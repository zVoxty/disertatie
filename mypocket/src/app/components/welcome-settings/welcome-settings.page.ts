import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { DataTransferService } from 'src/app/services/data-transfer.service';
import { ToastController } from '@ionic/angular';
import { SideMenuPage } from '../side-menu/side-menu.page';

@Component({
  selector: 'app-welcome-settings',
  templateUrl: './welcome-settings.page.html',
  styleUrls: ['./welcome-settings.page.scss'],
})
export class WelcomeSettingsPage implements OnInit {
  //sunt in form cu [(ngModel)] si o sa se seteze aici
  user = new User;
  monthlySaving: boolean;

  constructor(private router: Router, private userService: UserService,
    private dataTransferService: DataTransferService, private toastController: ToastController, private sideMenu:SideMenuPage) {
  }

  ngOnInit() {
  }

  async addUserBudgetSettings() {
    let currentDate = new Date();
    if (new Date(this.user.salaryDate).getTime() - currentDate.getTime() < 0) {
      const toast = await this.toastController.create({
        color: "warning",
        message: "Please pick a date that does not belongs to past.",
        duration: 2000
      });
      toast.present();
      return
    }

    this.userService.addUserBudgetSettings(this.user.salary, this.user.savedAmount, this.user.salaryDate).subscribe(async response => {
      this.sideMenu.ngOnInit();
      this.router.navigateByUrl('home')
      try {
        await this.userService.getUser();
      } catch (error) {
        console.log("Welcome settings failed add user budget settings");
      }
    })
  }

}
