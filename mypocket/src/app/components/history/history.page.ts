import { Component, OnInit } from '@angular/core';
import { Expense } from 'src/app/models/expense';
import { ExpensesService } from 'src/app/services/expenses.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
 selectedDate;
 expenses:Expense[]=[];

  constructor(private expensesService:ExpensesService) { }

  ngOnInit() {
  }

  showHistory(){
    debugger
    this.expensesService.getExpensesByDate(this.selectedDate).subscribe(response=>{
      this.expenses=response;
    })
  }


}
