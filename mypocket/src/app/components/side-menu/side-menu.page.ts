import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertController, MenuController, Platform, ToastController } from '@ionic/angular';
import { ExpenseCategory } from 'src/app/models/expensCategory';
import { AuthService } from 'src/app/services/auth.service';
import { BudgetService } from 'src/app/services/budget.service';
import { DataTransferService } from 'src/app/services/data-transfer.service';
import { ExpenseCategoryService } from 'src/app/services/expense-category.service';
import { ExpensesService } from 'src/app/services/expenses.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.page.html',
  styleUrls: ['./side-menu.page.scss'],
})
export class SideMenuPage implements OnInit, AfterViewInit {
  expenseCategories: ExpenseCategory[];
  spentAmountPerCategory: number;

  private searchbar;
  private items;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private expenseCategoriesProvider: ExpenseCategoryService,
    private authService: AuthService,
    private menuCtrl: MenuController,
    private router: Router,
    private expensesService: ExpensesService,
    private alertController: AlertController,
    private toastController: ToastController,
    private userService: UserService,
    private budgetService: BudgetService,
    private dataTransferService: DataTransferService
  ) {
    this.initializeApp();
    console.log("Side-menu initialized in constructor")

  }

  ngAfterViewInit(): void {
    this.searchbar = document.getElementById('searchBar');
  }

   async ngOnInit() {
    console.log('Side-menu initialized');
    const user = await this.userService.getUser();
    if (user != null) {
       this.getExpenseCategories();
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

  }

  showExpenseStatusesPage(category: ExpenseCategory) {
    this.expensesService.getSpentAmountOnCategory(category.id).subscribe(spentAmount => {
      this.spentAmountPerCategory = spentAmount;
      this.router.navigate(['/expense-statuses-card'],
        {
          queryParams: {
            expenseCategoryName: category.name,
            expenseCategoryDescription: category.description,
            expenseCategoryId: category.id,
            spentAmountPerCategory: this.spentAmountPerCategory
          }
        });

      this.menuCtrl.close();
    });
  }

  getExpenseCategories() {
    this.expenseCategoriesProvider.getExpenseCategories()
      .subscribe(expenseCategories => {
        this.expenseCategories = expenseCategories;
        this.searchbar.addEventListener('ionInput', this.handleInput);
      });
  }

  handleInput(event) {
    const query = event.target.value.toLowerCase();

    if (!this.items) {
      let itemsP = document.getElementById('itemsParent');
      this.items = Array.from(itemsP.children);
    }

    requestAnimationFrame(() => {
      this.items.forEach((item: Element) => {
        const shouldShow = item.textContent.toLowerCase().indexOf(query) > -1;

        let displayText = shouldShow ? "block" : "none";
        let display = "display: " + displayText;
        item.setAttribute("style", display);
      });
    });
  }

  logout() {
    this.authService.logout();
    this.menuCtrl.close();
  }

  async home() {
    this.router.navigateByUrl('home')
    await this.userService.getUser();
    this.budgetService.getTotalSpentAmount().subscribe(totalSpentAmount => {
      if (totalSpentAmount != null) {
        this.dataTransferService.totalSpentAmount = Number(totalSpentAmount.toString());
      }
    })
    this.menuCtrl.close();
  }

  async addNewCategory() {
    const alert = await this.alertController.create({
      header: 'New category',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name...'
        },
        {
          name: 'description',
          type: 'text',
          placeholder: 'Description...'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'CREATE',
          handler: category => {
            this.expenseCategoriesProvider.addExpenseCategory(category.name, category.description)
              .subscribe(response => {
                if (response.toString() == "CREATED") {
                  this.getExpenseCategories();
                  this.presentAddedCategoryToast(response.toString());
                } else {

                }
              })
          }
        }
      ]
    });
    await alert.present();
  }


  async presentAddedCategoryToast(responseMessage: string) {
    let message;

    if (responseMessage == "CREATED") {
      message = "Category successfully added!";
    } else {
      message = "Could not add the category."
    }

    const toast = await this.toastController.create({

      message: message,
      duration: 2000
    });
    toast.present();
  }


}
