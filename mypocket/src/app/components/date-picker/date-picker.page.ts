import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Calendar, Day } from 'dayspan';
import * as moment from 'moment';
import { DataTransferService } from 'src/app/services/data-transfer.service';
import { ExpensePeriod } from 'src/app/models/expensePeriod';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.page.html',
  styleUrls: ['./date-picker.page.scss'],
})
export class DatePickerPage implements OnInit {
  @Input() monthLabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  @Input() dayLabels = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
  @Input() date: Date;
  @Input() fromDate: Date;
  @Input() toDate: Date;
  @Input() dateStyles: any = {};
  @Input() backgroundStyle = { 'background-color': '#ffffff' };
  @Input() dayLabelsStyle = { 'font-weight': 500, 'font-size': '14px' };
  @Input() monthLabelsStyle = { 'font-size': '15px' };
  @Input() yearLabelsStyle = { 'font-size': '15px' };
  @Input() itemSelectedStyle = { 'background': '#488aff', 'color': '#f4f4f4 !important' };
  @Input() todaysItemStyle = { 'color': '#32db64' };
  @Input() selectedExpensesCategoryName;

  weeks: Array<Array<Day>>;

  private _yearSelected: number;
  public get yearSelected(): number {
    return this._yearSelected;
  }
  public set yearSelected(v: number) {
    this._yearSelected = v;
  }


  private _monthSelected: number;
  public get monthSelected(): number {
    return this._monthSelected;
  }
  public set monthSelected(v: number) {
    this._monthSelected = v;
  }



  ngOnInit() {
    this.initialize();
  }

  constructor(private dataTransferService: DataTransferService, private toastController: ToastController) {

  }

  getPickedDates() {
    return this.dataTransferService.selectedDates;
  }

  initialize() {
    this.createCalendarWeeks();

    let dates = this.dataTransferService.selectedDates.slice();
    if (dates.length != 0) {
      // Get last date to set month, year
      let pickedDate = dates[dates.length - 1];

      this.yearSelected = pickedDate.getFullYear();
      this.monthSelected = pickedDate.getMonth();
    } else {
      let date = new Date();
      this.yearSelected = date.getFullYear();
      this.monthSelected = date.getMonth();
    }
  }

  createCalendarWeeks() {
    this.weeks = this.generateCalendarWeeks(
      Day.fromMoment(
        moment(this.monthSelected + '-01-' + this.yearSelected, 'MM-DD-YYYY')
      )
    );
  }

  hasPrevious(): boolean {
    if (!this.fromDate) {
      return true;
    }

    let previousMonth;
    let previousYear;
    if (this.monthSelected === 1) {
      previousMonth = 11;
      previousYear = this.yearSelected - 1;
    } else {
      previousMonth = this.monthSelected;
      previousYear = this.yearSelected;
    }

    // The last day of previous month should be greatar than or equal to fromDate
    return new Date(previousYear, previousMonth, 0) >= this.fromDate;
  }

  hasNext(): boolean {
    if (!this.toDate) {
      return true;
    }

    let nextMonth;
    let nextYear;
    if (this.monthSelected === 12) {
      nextMonth = 0;
      nextYear = this.yearSelected + 1;
    } else {
      nextMonth = this.monthSelected;
      nextYear = this.yearSelected;
    }

    // The first day of next month should be less than or equal to toDate
    return new Date(nextYear, nextMonth, 1) <= this.toDate;

  }

  previous() {
    if (this.monthSelected === 1) {
      this.monthSelected = 12;
      this.yearSelected--;
    } else {
      this.monthSelected--;
    }

    this.createCalendarWeeks();
  }

  next() {
    if (this.monthSelected === 12) {
      this.monthSelected = 1;
      this.yearSelected++;
    } else {
      this.monthSelected++;
    }

    this.createCalendarWeeks();
  }

  async selectDay(day: Day) {
    let date = new Date(day.toDate());
    date.setMonth(this.monthSelected);

    let currentDate = new Date();
    if (date.getTime() - currentDate.getTime() < 0) {
      if (date.getDate() != currentDate.getDate()) {
          const toast = await this.toastController.create({
            color: "warning",
            message: "Cannot pick a date that belongs to past.",
            duration: 2000
          });
          toast.present();
          return  
      }
    }

    let exists = this.dataTransferService.selectedDates.find(selDate => selDate.toDateString() === date.toDateString())
    if (!exists) {
      this.dataTransferService.selectedDates.push(date);
    } else {
      this.removeDate(date);
    }
  }

  generateCalendarWeeks(forDay: Day): Array<any> {
    const weeks: Array<any> = [];
    const month = Calendar.months<string, any>(1, forDay);
    const numOfWeeks = month.days.length / 7;

    let dayIndex = 0;
    for (let week = 0; week < numOfWeeks; week++) {
      const days = [];
      for (let day = 0; day < 7; day++) {
        days.push(month.days[dayIndex]);
        dayIndex++;
      }
      weeks.push(days);
    }
    return weeks;
  }

  fetchDays() {
    if (this.dataTransferService.selectedDates.length === 0) {
      return "";
    }

    let toRet = "";
    this.dataTransferService.selectedDates.forEach((date: Date) => {
      toRet += date.getDate() + " ";
    });

    return toRet;
  }

  dayAdded(day: Day) {
    let dateIt = new Date(day.toDate());
    dateIt.setMonth(this.monthSelected);
    dateIt.setFullYear(this.yearSelected);

    if (this.dataTransferService.selectedDates.length === 0) {
      return false;
    }

    let exists = false;
    this.dataTransferService.selectedDates.forEach((date: Date) => {
      if (date.getFullYear() === dateIt.getFullYear() && date.getMonth() === dateIt.getMonth() && date.getDate() === dateIt.getDate()) {
        exists = true;
      }
    });

    return exists;
  }

  removeDate(date: Date) {
    let index = this.dataTransferService.selectedDates.indexOf(date);
    this.dataTransferService.selectedDates.splice(index, 1);
  }
}
