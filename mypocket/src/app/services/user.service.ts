import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { take, map } from 'rxjs/operators';
import { User } from '../models/user';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DataTransferService } from './data-transfer.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  addUserBudgetSettingsUrl = environment.appApiUrl + "/mypocket/user/addUserBudgetSettings";
  getCurretBugdetUrl = environment.appApiUrl + "/mypocket/user/currentBudget";
  getUserUrl = environment.appApiUrl + "/mypocket/user"
  addSalaryToUserUrl = environment.appApiUrl + "/mypocket/user/addSalary";

  username: string;

  constructor(private http: HttpClient, private storage: Storage, private dataTransferService: DataTransferService) {
    this.setUsername();
  }

  addUserBudgetSettings(amount: number, savedAmount: number, salaryDate: Date) {
    return this.http.post(this.addUserBudgetSettingsUrl, {
      'username': this.username,
      'salary': amount,
      'savedAmount': savedAmount,
      'salaryDate': salaryDate
    }).pipe(
      take(1),
      map((response) => {
        return response;
      }))
  }

  async getUser() {
    await this.setUsername();

    if (!!this.username) {
      try {
        let user = await this.http.get<User>(this.getUserUrl + "/" + this.username).toPromise();

        this.dataTransferService.currUser = user;

        return this.dataTransferService.currUser;
      } catch (error) {
        console.log("Cannot get the user.");
      }
    }
    return null;
  }

  addSalaryToUser() {
    //adauga salariul si returneaza ok
    return this.http.get<Response>(this.addSalaryToUserUrl + "/" + this.username)
  }

  async setUsername() {
    await this.storage.get('username').then(response => {
      this.username = response;
    });
  }
}
