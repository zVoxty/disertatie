import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Expense } from '../models/expense';
import { take, map, switchMap, catchError } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { User } from '../models/user';
import { Observable, throwError } from 'rxjs';
import { stringify } from 'querystring';
import { UserService } from './user.service';
import { DataTransferService } from './data-transfer.service';
import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';




@Injectable({
  providedIn: 'root'
})
export class ExpensesService {
  saveExpenseUrl = environment.appApiUrl + "/mypocket/expense/save";
  getExpensesByCategoryAndStatusUrl = environment.appApiUrl + "/mypocket/expense/findAll";
  makePaymentUrl = environment.appApiUrl + "/mypocket/expense/makePayment";
  deleteExpenseUrl = environment.appApiUrl + "/mypocket/expense/delete";
  getMandatoryUnpaidExpensesAfterPaymendDateUrl = environment.appApiUrl + "/mypocket/expense/mandatoryUnpaidExpensesAfterPaymendDate";
  getSpentAmountOnCategoryUrl = environment.appApiUrl + "/mypocket/user/budget/spentAmount";
  authenticationToken: string;
  getExpensesByDateUrl = environment.appApiUrl + "/mypocket/expense/date";
  constructor(private http: HttpClient,
    private dataTransferService: DataTransferService,
    private storage: Storage) {
  }

  public init() {
  }

  save(expense: Expense) {
    return this.http.post(this.saveExpenseUrl, expense);
  }

  getExpensesByCategoryAndStatus(category: string, status: string): Observable<Array<Expense>> {
    return this.http.get<Array<Expense>>(this.getExpensesByCategoryAndStatusUrl + "/" + category + "/" + status);
  }


  deleteExpense(expenseId: number) {
    console.log(expenseId)
    return this.http.get(this.deleteExpenseUrl + "/" + expenseId);
  }

  makePayment(expenseId: number, amount: number) {
    return this.http.post(this.makePaymentUrl, { 'expenseId': expenseId, 'amount': amount });
  }

  getMandatoryUnpaidExpensesAfterPaymendDate(username: string): Observable<Array<Expense>> {
    return this.http.get<Array<Expense>>(this.getMandatoryUnpaidExpensesAfterPaymendDateUrl + "/" + username);
  }

  getSpentAmountOnCategory(categoryId:number): Observable<number> {
    return this.http.get<number>(this.getSpentAmountOnCategoryUrl + "/" + categoryId);
  }

  getExpensesByDate(selectedDate) {
    var dateFormat = selectedDate.split('T')[0]; 

    return this.http.get<Expense[]>(this.getExpensesByDateUrl + "/" + dateFormat);
  }

  getStatusValueForServer(value: string): string {
    let statusValue = "";
    switch (value) {
      case "Paid":
        statusValue = "PAID";
        break;
      case "Unpaid":
        statusValue = "UNPAID"
        break;
     
      default:
        break;
    }
    return statusValue;
  }
}
