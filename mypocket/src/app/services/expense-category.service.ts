import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ExpenseCategory } from '../models/expensCategory';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ExpenseCategoryService {
expenseCategoriesUrl = environment.appApiUrl + "/mypocket/expense/categories/all";
addExpenseCategoryUrl=environment.appApiUrl + "/mypocket/expense/categories/add";
deleteExpenseCategoryUrl=environment.appApiUrl + "/mypocket/expense/categories/delete";

constructor(public http: HttpClient) {
}

 getExpenseCategories(){
  return  this.http.get<ExpenseCategory[]>(this.expenseCategoriesUrl);
}


addExpenseCategory(name:string,description:string){
  return this.http.post(this.addExpenseCategoryUrl,{'name':name, 'description':description});
}

deleteExpenseCategory(id: number) {
  return this.http.get(this.deleteExpenseCategoryUrl+"/"+ id);
}

}
