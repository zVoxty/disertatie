import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BudgetService {
  totalSpentAmountUrl=environment.appApiUrl + "/mypocket/user/budget/totalSpentAmount";

  constructor(public http: HttpClient) { }

  public getTotalSpentAmount(){
    return this.http.get(this.totalSpentAmountUrl);
  }
}
