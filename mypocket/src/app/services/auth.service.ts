import { Platform, ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable, from, of } from 'rxjs';
import { take, map, switchMap, catchError } from 'rxjs/operators';
import { JwtHelperService } from "@auth0/angular-jwt";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';

const helper = new JwtHelperService();
const TOKEN_KEY = 'jwt-token';
 const USERNAME_KEY='username';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user: Observable<any>;
  private userData = new BehaviorSubject(null);
  

  loginUrl = environment.appApiUrl + "/mypocket/auth/login";
  registerUrl = environment.appApiUrl + "/mypocket/auth/register";

  constructor(private storage: Storage, private http: HttpClient, private plt: Platform, private router: Router,
              private toastController: ToastController) {
    this.loadStoredToken();
  }

  register(user: User) {
    return this.http.post(this.registerUrl, user);
  }

  login(username: string, password: string) {
    try {
      return this.http.post(this.loginUrl, { username: username, password: password }).pipe(
        take(1),
        map((res) => {
          return res;
        }),
        switchMap(result => {  //primesc map-ul de la api ce contine si token-ul pentru autentificare
          let decoded = helper.decodeToken(result['token']);
          this.userData.next(decoded);
          let storageObs = from(this.storage.set(TOKEN_KEY, result['token']));
          this.storage.set(USERNAME_KEY,result['username']);
          return storageObs;
        }),
        catchError(async (err: HttpErrorResponse) => {
          if (err.status === 403) {
            const toast = await this.toastController.create({    
              color: "warning",
              message: "Wrong credentials provided.",
              duration: 2000
            });
            toast.present();
          }
          const apiError = "x"
          return Observable.throw(apiError);
        })
      );
    } catch (error) {
      console.log("errr")
    }
  }
  
  logout() {
    this.storage.remove(USERNAME_KEY);

    this.storage.remove(TOKEN_KEY).then(() => {
      this.router.navigateByUrl('/');
      this.userData.next(null);
    });
  }

  getUser() {
    return this.userData.getValue();
  }

  loadStoredToken() {
    let platformObs = from(this.plt.ready());

    this.user = platformObs.pipe(
      switchMap(() => {
        return from(this.storage.get(TOKEN_KEY));
      }),
      map(token => {
        if (token) {
          let decoded = helper.decodeToken(token);
          this.userData.next(decoded);
          return true;
        } else {
          return null;
        }
      })
    );
  }
}
