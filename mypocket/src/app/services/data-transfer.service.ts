import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { BehaviorSubject } from 'rxjs';
import { ExpensePeriod } from '../models/expensePeriod';
import { Expense } from '../models/expense';

@Injectable({
  providedIn: 'root'
})
export class DataTransferService {
  private _user = new User();
  private _expense = new Expense();
  private _selectedExpense = new Expense();
  private _expenses = new Array<Expense>();
  private _expensePeriods = new Array<ExpensePeriod>();
  private _spentAmount: number;
  private _totalSpentAmount: number
  public get spentAmount(): number {
    return this._spentAmount;
  }
  public set spentAmount(spentAmount: number) {
    this._spentAmount = spentAmount;
  }

  public get totalSpentAmount() {
    return this._totalSpentAmount;
  }
  public set totalSpentAmount(totalSpentAmount: number) {
    this._totalSpentAmount = totalSpentAmount;
  }

  public get User() {
    return this._user;
  }
  public set User(user: User) {
    this._user = user
  }


  public get expensePeriods() {
    return this._expensePeriods;
  }
  public set expensePeriods(expensePeriods: Array<ExpensePeriod>) {
    this._expensePeriods = expensePeriods;
  }


  public get selectedExpense() {
    return this._selectedExpense;
  }
  public set selectedExpense(selectedExpense: Expense) {
    this._selectedExpense = selectedExpense;
  }


  public get expense() {
    return this._expense;
  }
  public set expense(expense: Expense) {
    this._expense = expense;
  }
  public get expenses() {
    return this._expenses;
  }
  public set expenses(expenses: Array<Expense>) {
    this._expenses = expenses;
  }



  private _selectedDates: Date[] = [];

  public get selectedDates(): Date[] {
    return this._selectedDates;
  }
  public set selectedDates(v: Date[]) {
    this._selectedDates = v;
  }

  private _currUser: User = new User();
  public get currUser(): User {
    return this._currUser;
  }

  public set currUser(v: User) {
    this._currUser = v;
  }

  
  private _contextExpense : Expense;
  public get contextExpense() : Expense {
    return this._contextExpense;
  }
  public set contextExpense(v : Expense) {
    this._contextExpense = v;
  }

  constructor() { }
}
