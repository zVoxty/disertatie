export class ExpensePeriod {
    id:number;
    date: Date;
    payed:boolean;
    
    constructor(date: Date) {
        this.date = date;
    }
}