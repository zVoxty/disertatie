import { ExpensePeriod } from './expensePeriod';
import { ExpenseCategory } from './expensCategory';
import { User } from './user';

export class Expense {
    id: number;
    title: string;
    amount: number;
    status: string;
    category: ExpenseCategory;
    dates: Array<Date>;
    mandatory: boolean;
    user: User;
    expensePeriods: ExpensePeriod[];
    description: string;
    paymentDate:Date;

    constructor() {
        this.user = new User();
        this.category = new ExpenseCategory();
        this.expensePeriods = new Array();
    }
}