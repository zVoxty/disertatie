export class User{
    public username : string;

    public password : string;
    
    public email : string;
    
    public salary : number;
    
    public salaryDate : Date;
    
    public amount : number = 0;
    
    public savedAmount : number= 0;
    
    public totalSpentAmount : number= 0;
}